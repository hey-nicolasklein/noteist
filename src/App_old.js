import React, { useEffect, useReducer, useState } from 'react';
import logo from './logo.svg';
import './App.css';


const baseUrl = 'https://asdfasdfasdfasdfasdf-rest.herokuapp.com/notes'
const localURL = 'http://localhost:3003/notes'


function reducer(state, action){
  switch(action.type){
    case 'addNote': 
        return {count: state.count + 1, notes: [... state.notes, {
          title: action.payload.heading, 
          text: action.payload.text,
          checked: false}]}
    case 'toggle':
        return{...state, notes: state.notes.map((item, idx) =>{
          if(action.payload === idx){
            return {
              title: item.heading,
              text: item.text,
              checked: !item.checked
            }
          }
          return item
          })};
    case 'loadNotes':
        const newData = action.notes;
        return{
          count: action.notes.length,
          notes: action.notes
        }
        
    default: return
  }
}

const NoteItem = ({note, onClick}) => {
  return (
    <div id="noteItem"onClick={onClick}>
      <h3>{note.title}</h3>
      <h5>{note.text}</h5>
    </div>)
}


const App = () => {

  const [data, dispatch] = useReducer(reducer, {
    notes: [
      {title: "hi", text:"its me", checked: "false"},
      {title: "Who are you?", text: "I know you", checked: true}
    ], 
    count: 0
  });
  const [title, changeTitle] = useState("");
  const [text, changeText] = useState("");

  useEffect(() => {
    const request = new Request(`${localURL}`);
    fetch(request).then(response => response.json()).then(myJson => {
      console.log(myJson);
      dispatch({type: 'loadNotes', notes: myJson});
    });

  }, [])

  function handleClick(idx){
    dispatch({type: 'toggle', payload: idx})
  }

  

  function renderNote(note, idx){
    return (<NoteItem onClick={() => handleClick(idx)} key={idx} note={note}/>)
  }


  return (
    <div id="container">
      <div id="addNote">
        <div id="addNoteCard">
          <h1>new Note</h1>
          <p>Note count: {data.count}</p>
          <form onSubmit={
            (e) => {  
              e.preventDefault();
              changeTitle("");
              changeText("");
              if(text.length > 0 || title.length > 0){
                dispatch({type: "addNote", payload: {title: title, text: text}});
              }
            }
          }>
            <div id="noteInput">
              <input type="text" placeholder="Title" value={title} onChange={(e) => changeTitle(e.target.value)}/>
              <textarea value={text} placeholder="Notiz" onChange={(e) => changeText(e.target.value)}/>
            </div>
            <button type="submit" value="submit">Submit</button>
          </form>
        </div>
      </div>
      <div id="displayNotes">
        {data.notes.map((note, idx) => {
          return renderNote(note, idx)
        })}
      </div>
    </div>
  );
}



export default App;
