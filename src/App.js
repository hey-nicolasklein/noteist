import React, { useCallback, useEffect, useReducer, useState } from 'react';
import './App.css';

//RESTful api adress
const localURL = process.env.REACT_APP_REST_URL;

//Handles the local database state and local changes to it
//such as POST, PATCH, DELETE
function reducer(state, action){
  switch(action.type){
    case 'loadNotes':
      return {
        notes: action.notes,
        count: action.notes.length
      }
    case 'addNote':
      return {
        notes: [...state.notes, action.note],
        count: state.count + 1
      }
    case 'deleteNote':
      console.log('now deleting'+action.id)
      const newState = [...state.notes].filter(note => {
        const result = (note._id === action.id);
        return !result;
      });
      console.log(newState);
      return {
        notes: newState,
        count: state.count
      }
    case 'flipNote':
      console.log('Flipping Note with id '+action.id);
      return {
        notes: state.notes.map(e => e._id === action.id ? {...e, done: !e.done} : e),
        count: state.count
      }
    
        
    default: return
  }
}

//Note Component visually representing one note.
//Contains a button to delete it.
//If clicked anywhere else on it it flips its "done" status.
const NoteItem = ({note, onClick, onDone}) => {
  return (
    <div onClick={onDone} className={"noteItem "+(note.done ? 'noteCompleted' : '')}>
      <h3>{note.title}</h3>
      <h5>{note.text}</h5>
      <h6>{note._id}</h6>
      <button className="delButton" onClick={onClick}>X</button>
    </div>)
}


const App = () => {

  const [data, dispatch] = useReducer(reducer, {
    notes: [], 
    count: 0
  });
  const [title, changeTitle] = useState("");
  const [text, changeText] = useState("");

  //Handles fetch api errors 
  function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
  }

  //Loads all notes from the rest api and replaces all local notes
  const refreshNotes = useCallback(()=> {
    const request = new Request(`${localURL}`);
    fetch(request)
      .then(handleErrors)
      .then(response => response.json())
      .then(content => dispatch({type: 'loadNotes', notes: content}))
      .catch(error => console.err(error));
  },[]);

  //Adds a new note to the local state and posts it to the backend
  const addNote = () => {
    const newNote = {
      'title': title,
      'text': text
    }

    const init = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newNote)
    }


    const request = new Request(`${localURL}`, init);
    fetch(request)
      .then(handleErrors)
      .then(response => response.json())
      .then(content => {
        console.log('Success:', content);
        dispatch({type: 'addNote', note: content});
      })
      .catch(error => console.error('Error:', error));
  }

  //Deletes a local note and sends delete request
  const deleteNote = (id) => {
    console.log(id);

    const init = {
      method: 'DELETE',
    }

    const request = new Request(`${localURL}/${id}`, init);
    fetch(request)
      .then(handleErrors)
      .then(response => response.json())
      .then(content => {
        console.log('Success:', content);
        dispatch({type: 'deleteNote', id: id});
      })
      .catch(error => console.error('Error:', error));
  }

  //Flips the "done" state of a note and sends a PATCH request to the backend
  const flipNote = (id) => {
    console.log(id);

    const status = data.notes.find(e => e._id === id).done;

    console.log(status);


    const newStatus = {
      'done': !status
    }

    const init = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newStatus)
    }


    const request = new Request(`${localURL}/${id}`, init);
    fetch(request)
      .then(handleErrors)
      .then(response => response.json())
      .then(content => {
        console.log('Success:', content);
        dispatch({type: 'flipNote', id: id});
      })
      .catch(error => console.error('Error:', error));

  }

  //Loads all of the backend's notes at application mount
  useEffect(() => refreshNotes(), [refreshNotes])

  function renderNote(note){
    return (<NoteItem onDone={() => flipNote(note._id)} onClick={() => deleteNote(note._id)} key={note._id} note={note}/>)
  }

  //Runs when the submit button is pressed.
  //Adds a new Note containing the inputed information and POSTS it to the backend.
  function onSubmit(e){
    e.preventDefault();

    if(text.length > 0 || title.length > 0){
      //dispatch({type: "addNote", payload: {note: note}});
      addNote();
    }
    changeTitle("");
    changeText("");
  }

  return (
    <div>
    <div id="container">
      <div id="addNote">
        <div id="addNoteCard">
          <h1>new Note</h1>
          <p>Note count: {data.count}</p>
          <form onSubmit={e => onSubmit(e)}>
            <div id="noteInput">
              <input type="text" placeholder="Title" value={title} onChange={(e) => changeTitle(e.target.value)}/>
              <textarea value={text} placeholder="Notiz" onChange={(e) => changeText(e.target.value)}/>
            </div>
            <button type="submit" value="submit">Submit</button>
          </form>
        </div>
      </div>
      <div id="displayNotes">
        {
          data.notes.map(e => renderNote(e))
        }
      </div>
    </div>
    </div>
  );
}



export default App;
